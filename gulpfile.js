var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var notify = require('gulp-notify')
var plumber = require('gulp-plumber');
var pug = require('gulp-pug');

gulp.task('browser-sync', ['sass', 'pug'], function(){
	browserSync.init({
		server: {baseDir: "build/"}
	});

	gulp.watch('app/sass/**/*.scss', ['sass']);
	gulp.watch('app/pug/**/*.pug', ['pug']);
});

gulp.task('sass', function(){
	return gulp.src('app/sass/**/*.scss')
	.pipe(plumber({
		errorHandler: notify.onError(function(err){
			return{
				title: 'Styles',
				message: err.message
			}
		})
	}))
	.pipe(sass(
	// {
	// 	outputStyle: 'compressed'
	// }
	))
	.pipe(gulp.dest('build/css'))
	.pipe(browserSync.stream());
});

gulp.task('pug', function buildHTML(){
	return gulp.src('app/pug/index.pug')
	.pipe(plumber({
		errorHandler: notify.onError(function(err){
			return{
				title: 'Styles',
				message: err.message
			}
		})
	}))
	.pipe(pug(
	// {
	// 	pretty: true
	// }
	))
	.pipe(gulp.dest('build/'))
	.pipe(browserSync.stream());
});

gulp.task('default', ['browser-sync'], function(){
	gulp.src("app/img/**/*")
	.pipe(gulp.dest("build/img"));
});